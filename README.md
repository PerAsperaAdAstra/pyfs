# PyFS README

## Introduction
Python Flight Software (PyFS) is intended to (crudely) replicate
the basic functionality of core Flight System (CFS), including 
its basic directory/file structure, scheduler, and more, but in Python.

It's hoped that this can be used as a quick and dirty way to 
get software infrastructure running which can then be relatively
easily translated to proper CFS.  PyFS attempts to do this by preserving
parts like the file structure, Makefile, and scheduler inputs.

There's no guarantee that this is comprehensive or done the right way...

Good luck!

## Overview

### Quick-Start for Running
To start PyFS, simply install the dependencies and then run the /build/exe/main.py script with Python 3.

### Description
Description of the components of PyFS in alphabetical order of their top-level directories
in the top-level directory:

1. apps
    * This directory houses all the applications.
2. build
    * This directory houses the Makefile and the main executable.
3. tools
    * This directory contains basic tools, including a script to stop all Python processes.
4. .gitignore
    * This file tells Git what to ignore.
5. LICENSE
    * This is the license (MIT).
6. README.md
    * The README.

## References
* https://github.com/nasa/cFS
