#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
hk_app_header


'''

###########################
#IMPORT MODULES
###########################
import zmq
from pyfs.build.build_defs.build_config_file import *


###########################
#CLASSES
###########################
# used to store general stateful data about the class
class hk_workspace(object):
    def __init__(self):
        self.dummy_stateful_variable = int

# used to store data related to communication with other apps
class hk_bus(object):
    def __init__(self):
        # start ZMQ client for SCH
        self.wakeup_context = zmq.Context()
        self.wakeup_socket = self.wakeup_context.socket(zmq.SUB)
        self.wakeup_socket.setsockopt(zmq.SUBSCRIBE, b"")
        self.wakeup_socket.connect("tcp://" + host_address + ":" + str(sch_app_port))

        # start ZMQ client for receiving commands
        self.command_context = zmq.Context()
        self.command_socket = self.command_context.socket(zmq.SUB)
        self.command_socket.setsockopt(zmq.SUBSCRIBE, b"")
        self.command_socket.connect("tcp://" + host_address + ":" + str(hk_app_cmd_in_port))

        # start ZMQ client for receiving hk from other apps
        self.hk_in_context = zmq.Context()
        self.hk_in_socket = self.hk_in_context.socket(zmq.SUB)
        self.hk_in_socket.setsockopt(zmq.SUBSCRIBE, b"")
        self.hk_in_socket.bind("tcp://" + host_address + ":" + str(hk_app_in_port))

        # start ZMQ client for Publishing OutData
        self.out_data_context = zmq.Context()
        self.out_data_socket = self.out_data_context.socket(zmq.PUB)
        self.out_data_socket.bind("tcp://" + host_address + ":" + str(hk_app_outdata_port))

# stores relevant paths for class

# stores constant values for class
class hk_constants(object):
    def __init__(self):
        self.APP_NAME_STR = "HK_APP"

class hk_error_codes(object):
    def __init__(self):
        self.NOT_INITIALIZED = 0
        self.NO_ERROR = 1
        self.GENERAL_FAILURE = 2
        self.DUMMY_ERROR_TYPE = 3



