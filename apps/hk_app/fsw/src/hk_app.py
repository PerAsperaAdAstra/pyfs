#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
hk app

 This is the highest level representation of your app
 Make other files and classes that are contained within or called from this one. Don't bloat this file!
 Philosophically, this file should mainly call other functions or do basic interfacing operations
 In CFS, this file would be responsible for initialization, processing data and commands, calling the main loop, and sending the outdata/housekeeping.
 If you are doing more than that here, consider moving it to another file and calling it from this one.
 In actual CFS, this file would probably not be a class, it would be a c file.
 Since python doesn't have a way to scope variables to a file, we're using a class here to prevent having a ton of globals

'''

###########################
#IMPORT MODULES
###########################
import os
import sys
import zmq
import json

# import "header" file
top_dir = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))))))
sys.path.append(top_dir)
from pyfs.apps.hk_app.fsw.src.hk_app_header import *
from pyfs.apps.hk_app.fsw.mission_inc.hk_app_msg import *
from pyfs.apps.utils import sch_handling, base_paths
from pyfs.apps.hk_app.fsw.src.hk_combined_manager import hk_combined_manager


###########################
#CLASSES
###########################
class hk_app(object):
    def __init__(self):
        # common members, imported from utils
        self.paths = base_paths.base_paths(os.path.realpath(__file__))
        self.sch_handling = sch_handling.sch_handling()

        # app specific members, similar but different enough to warrant their own implementations
        # The workspace is used for information that will change overtime but you want it to persist between wakeups
        self.workspace = hk_workspace()
        self.bus = hk_bus()
        self.error_codes = hk_error_codes()
        self.hk_data = hk_hk_data()
        self.constants = hk_constants()
        self.hk_combined_manager = hk_combined_manager()

        #load input params
        iload_filename = os.path.join(self.paths.table_dir, 'iloads.json') #iload file name
        iload_file = open(iload_filename,)
        self.iloads = json.load(iload_file)

        # if you have a failure, set this to false
        self.init_success = True

    def main(self):
        #if init success, start main loop
        if self.init_success:
            self.receive_msg()
        else:
            print("HK_APP: Error initializing ")

    def receive_msg(self):

        #this is the actual main loop of the program
        while True:
            # code will pause here until it receives a message from the socket
            message = self.bus.wakeup_socket.recv()
            # parses message when received, may consider using different wakeup sockets in the future to reduce these checks
            sch_msg_cmd, sch_cmd_timing = message.decode('utf-8').split('_FREQUENCY_')
        
            if sch_msg_cmd == 'SCH_GROUP_HK_WAKEUP':
                # Before executing wakeup calls, make sure we're not overrunning
                parsed_msg = self.sch_handling.sch_protect_wakeup_overrun(message, sch_msg_cmd, sch_cmd_timing, self.sch_handling.LARGE_FREQUENCY_EPSILON )
                if(parsed_msg.sch_msg_cmd == 'SCH_GROUP_HK_WAKEUP'):
                    print("HK_APP: WAKEUP")
                    self.process_commands()
                    ret_val = self.process_data()
                    self.out_data = self.executive()
                    self.send_out_data()

    def process_data(self):
        # process latest output until the pipe is empty
        #print("HK_APP: process data")
        try:
            while True:
                [message_sender, message] = self.bus.hk_in_socket.recv_multipart(zmq.DONTWAIT)
                message_sender_str = message_sender.decode('utf-8')
                message_json = message.decode('utf-8')
                self.hk_combined_manager.update_combined_hk(message_sender_str, message_json)

                #print("HK_APP: updated combined entry for: " + message_sender_str)

        except zmq.error.Again:
            #print("HK_APP: woke up with no new data")
            pass
        return 0

    def process_commands(self):
        #print("HK_APP: process commands")
        try:
            message = self.bus.command_socket.recv(zmq.DONTWAIT)
            if message.decode('utf-8') == "HK_NOOP":
                print("HK_APP: received No-Op command")
            #TODO: This is where you would process commands
        except zmq.error.Again:
            pass
        return 0

    def executive(self):
        #TODO: add stuff
        #print("HK_APP: process executive")

        executive_out = 0
        return executive_out

    # Convert outdata class fields into a json dict and send across the software bus
    # This is where you would do any final post processing as well.
    # Alternatively, if you have a lot of post processing, make a "composer" class/function
    def send_out_data(self):
        if self.hk_combined_manager.combined_packet_dict:
       
            # Adds hk app's hk data to combined hk packet
            self.hk_data.heartbeat = self.hk_data.heartbeat + 1
            self.hk_combined_manager.combined_packet_dict["HK_APP"] = vars(self.hk_data)

            # outputs combined hk data
            json_str = json.dumps(self.hk_combined_manager.combined_packet_dict, default=vars)
            self.bus.out_data_socket.send(bytes(json_str, 'utf-8'))
            #print("HK_APP: send combined hk data")
            #print("HK_APP: "+str(json_str))

            # Reset error code after reporting housekeeping
            self.hk_data.curent_error_code = self.error_codes.NO_ERROR


###########################
# MAIN CODE
###########################

if __name__ == "__main__":
    hk_app = hk_app()
    hk_app.main()
