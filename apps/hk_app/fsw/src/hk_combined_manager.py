#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
hk_combined_manager


'''


###########################
#IMPORT MODULES
###########################
import json

# Import build config generated at runtime and shared def app names
from pyfs.build.build_defs.build_config_file import built_apps

# Import app hk data for each application being used
from pyfs.apps.demo_app.fsw.mission_inc.demo_app_msg import demo_hk_data
from pyfs.apps.hk_app.fsw.mission_inc.hk_app_msg import hk_hk_data


###########################
#CLASSES
###########################
class hk_combined_manager(object):
    def __init__(self):
        self.combined_packet_dict = {}
        self.initialize_combined_packet()


    # Loops through all built apps and creates a python dictionary of fields for each app.
    # That dictionary is then stored in the combined_packet_dictionary where the key is the app name, so a dict of dicts.
    def initialize_combined_packet(self):
        for app_str in built_apps:
            # TODO include SCH hk if we want that here
            if app_str == "SCH_APP":
                continue
            else:
                app_hk_object = hk_combined_single_entry(app_str)
                self.combined_packet_dict[app_str] = app_hk_object.app_hk_packet_dict
                print("HK_APP: app hk added to  " + app_str + ": "+ str(self.combined_packet_dict[app_str]))

    def update_combined_hk(self, app_str_name, json_hk_received):
        if(app_str_name.lower() in built_apps):
            self.combined_packet_dict[app_str_name] = json.loads(json_hk_received)
        else:
            print("HK_APP: hk received from app that is not in the build list:" + app_str_name)

class hk_combined_single_entry(object):
    def __init__(self, app_str_name):
        self.app_hk_packet_dict = {}
        if app_str_name in built_apps:
            if app_str_name == "demo_app":
                self.app_hk_packet_dict = vars(demo_hk_data())
            elif app_str_name == "hk_app":
                self.app_hk_packet_dict = vars(hk_hk_data())
            elif app_str_name == "sch_app":
                pass
                # TODO: implement SCH Hk and add here if we want that
            else:
                print("HK_APP: Error: app_str_name built that does not have a structure mapping in hk app: " + app_str_name)
                
        else:
            print("HK_APP: "+app_str_name + " in not in built_apps, double check app_names file and build_apps match strings!")
