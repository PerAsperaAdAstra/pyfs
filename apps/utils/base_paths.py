#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
base_paths

'''


###########################
#IMPORT MODULES
###########################
import os


###########################
#CLASSES
###########################
class base_paths(object):
    # pass os.path.realpath(__file__) into this constructor
    def __init__(self, filepath):
        self.src_dir = os.path.abspath(os.path.dirname(filepath)) #src dir name
        self.fsw_dir = os.path.abspath(os.path.dirname(self.src_dir)) #fsw dir name
        self.table_dir = os.path.join(self.fsw_dir, 'tables') #table dir name
        self.app_dir = os.path.abspath(os.path.dirname(self.fsw_dir)) #app dir name
        self.apps_dir = os.path.abspath(os.path.dirname(self.app_dir)) #apps dir name
        self.util_dir = os.path.join(self.apps_dir, 'utils') # utils dir
        self.pyfs_dir = os.path.abspath(os.path.dirname(self.apps_dir)) #pyfs dir name
        self.flight_dir = os.path.abspath(os.path.dirname(self.pyfs_dir)) #flight dir name
        self.base_dir = os.path.abspath(os.path.dirname(self.flight_dir)) #base dir name
        self.data_dir = os.path.join(self.base_dir, 'data')
        self.shared_defs_dir = os.path.join(self.base_dir, "shared_defs")

        if not os.path.exists(self.data_dir):
            os.mkdir(self.data_dir)
