#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
sch_handling

'''


###########################
#IMPORT MODULES
###########################
import time


###########################
#CLASSES
###########################
class parsed_message_structure(object):
    def __init__(self):
        self.sch_msg_cmd = "NONE"
        self.sch_cmd_freq = "0"
        self.sch_cmd_rem = "0"

class sch_handling(object):
    def __init__(self):
        self.last_hk_call_time=0  # initializing to 0 forces the first call to pass
        self.last_wakeup_call_time=0  # initializing to 0 forces the first call to pass
        self.app_wakeups_missed=0
        self.app_hk_missed=0
        self.SMALL_FREQUENCY_EPSILON = 0.002 # Equal to 1 SCH slot, prevents 
        self.LARGE_FREQUENCY_EPSILON = 0.01 # Equal to 5 SCH slots, used for multi-second frequency periods (HK Tlm)

    def sch_protect_hk_overrun(self, message, sch_msg_cmd, sch_cmd_timing, freq_epsilon=None):
        if freq_epsilon == None:
            freq_epsilon = self.LARGE_FREQUENCY_EPSILON

        parsed_message = parsed_message_structure()
        parsed_message.sch_msg_cmd = sch_msg_cmd
        
        # Parse out timing command into frequency and remainder allocations
        parsed_message.sch_cmd_freq, parsed_message.sch_cmd_rem = sch_cmd_timing.split('_REMAINDER_')

        # SCH_GROUP_APPNAME_HK or SCH_GROUP_APPNAME_WAKEUP
        split_cmd_str = parsed_message.sch_msg_cmd.split('_')

        if(split_cmd_str[-1] == "HK"):
            # Easy optimization, only call time once
            currentTime = time.time()
            # If the app has been called outside of the min frequency limit, or if this is the first time the app is called run the script
            # Also use an epsilon value to allow for noise on the sch message sending equal to 1 sch slot
            if (currentTime - self.last_hk_call_time > (float(parsed_message.sch_cmd_freq) - freq_epsilon)): 

                # Test this is working properly
                #timeDelta = currentTime - self.last_hk_call_time
                #print("SCH_HANDLING: hk Time delta between last call: " + str(timeDelta))

                # Parameter indicating the last time the app was called
                self.last_hk_call_time = currentTime
                # Parameter indicating how often the app has been called since the last time it ran
                self.app_hk_missed=0
    
            # If I'm not back in <frequency seconds>, just wait longer! 
            else:
                parsed_message.sch_msg_cmd = "NONE"
                parsed_message.sch_cmd_freq = "0"
                parsed_message.sch_cmd_rem = "0"
                self.app_hk_missed+=1
            return parsed_message
        else:
            print("SCH_HANDLING: Non-HK message passed to sch_protect_hk_overrun: " + str(split_cmd_str[-1]))
            parsed_message.sch_msg_cmd = "NONE"
            parsed_message.sch_cmd_freq = "0"
            parsed_message.sch_cmd_rem = "0"
            return parsed_message



    # executes code that prevents app from executing wakeups that are under it's minimum frequency value set in SCH
    def sch_protect_wakeup_overrun(self, message, sch_msg_cmd, sch_cmd_timing, freq_epsilon=None ):
        # Set default argument value to constant defined in sch_handling
        if freq_epsilon == None:
            freq_epsilon = self.SMALL_FREQUENCY_EPSILON

        parsed_message = parsed_message_structure()
        parsed_message.sch_msg_cmd = sch_msg_cmd
        
        # Parse out timing command into frequency and remainder allocations
        parsed_message.sch_cmd_freq, parsed_message.sch_cmd_rem = sch_cmd_timing.split('_REMAINDER_')

        # SCH_GROUP_DEMO_HK or SCH_GROUP_DEMO_WAKEUP
        split_cmd_str = parsed_message.sch_msg_cmd.split('_')
    
        if(split_cmd_str[-1] == "WAKEUP"):
            # Easy optimization, only call time once
            currentTime = time.time()
            # If the app has been called outside of the min frequency limit, or if this is the first time the app is called run the script
            # Also use an epsilon value to allow for noise on the sch message sending equal to 1 sch slot
            if (currentTime - self.last_wakeup_call_time >= (float(parsed_message.sch_cmd_freq) - freq_epsilon)):            
                
                # Test this is working properly
                #timeDelta = currentTime - self.last_wakeup_call_time
                #print("SCH_HANDLING: wakeup Time delta between last call: " + str(timeDelta))
                
                # Parameter indicating the last time the app was called
                self.last_wakeup_call_time = currentTime
                # Parameter indicating how often the app has been called since the last time it ran
                self.app_wakeups_missed=0
    
            # If I'm not back in <frequency seconds>, just wait longer! 
            else:
                parsed_message.sch_msg_cmd = "NONE"
                parsed_message.sch_cmd_freq = "0"
                parsed_message.sch_cmd_rem = "0"
                self.app_wakeups_missed+=1
            return parsed_message
        else:
            print("SCH_HANDLING: Non-wakeup message passed to sch_protect_wakeup_overrun: " + str(split_cmd_str[-1]))
            parsed_message.sch_msg_cmd = "NONE"
            parsed_message.sch_cmd_freq = "0"
            parsed_message.sch_cmd_rem = "0"
            return parsed_message


