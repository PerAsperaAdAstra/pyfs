#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
sch app

mean to broadly emulate CFS SCH

'''

###########################
#IMPORT MODULES
###########################
import os
import sys
import zmq
import time

top_dir = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))))))
sys.path.append(top_dir)
from pyfs.build.build_defs.build_config_file import *

###########################
# SUPPORT FUNCTIONS
###########################
def sch_app_main():

    #init
    sch_table = sch_app_init()
    #if init success, start main
    if sch_table:

        # start ZMQ server for SCH messages
        context = zmq.Context()
        socket = context.socket(zmq.PUB)
        socket.bind("tcp://" + host_address + ":" + str(sch_app_port))

        while True:

            print("SCH_APP: " + str(time.time()))
            for n in range(0,len(sch_table)-1):  #no idea why, but we're 2ms over our 1s

                if 'SCH_UNUSED' not in sch_table[n]:
                    #print("SCH_APP: Sending: "+ sch_table[n])
                    socket.send(bytes(sch_table[n], 'utf-8'))
                    #print("SCH_APP: sent msg")
                    #time.sleep(1)

                # sleeping for 2ms sleeps on every activity, not every slot! CFS would execute every activity in a slot in batches.
                #time.sleep(2.0/1000.0) #this is not good enough on Windows
                accurate_delay(2) #ms
                #print("SCH_APP: "+str(n) + " " + sch_table[n])


def sch_app_init():

    sch_table = sch_app_process_schedule_table()
    return sch_table


def sch_app_process_schedule_table():

    #parse nearby file for what apps to execute

    src_dir = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
    fsw_dir = os.path.abspath(os.path.dirname(src_dir))

    with open(os.path.join(fsw_dir,'tables','sch_def_schtbl.c'),'r+') as f:
        lines = f.readlines()

    sch_list = []
    for line in lines:
        if '  {  ' in line:
            #print("SCH_APP: "+str(line))
            if line[0] != '#' and line[0] != '/':
                parts = line.split(' ')
                line_no_spaces = ''.join(parts)
                line_parts = line_no_spaces.split(',')
                if line_parts[0] == 'SCH_UNUSED' or line_parts[1] != 'SCH_ACTIVITY_SEND_MSG':
                    sch_list.append('SCH_UNUSED')

                else:
                    sch_list.append(line_parts[5].strip('}')+'_FREQUENCY_'+str(line_parts[2])+'_REMAINDER_'+str(line_parts[3]))

    #print("SCH_APP: "+str(sch_list))

    return sch_list


def sch_app_process_command_messages():
    time.sleep(0.001)


def sch_app_process_commands():
    time.sleep(0.001)


def sch_app_report_housekeeping():
    time.sleep(0.001)


def accurate_delay(delay):
    #Function to provide accurate time delay in millisecond
    _ = time.perf_counter() + delay/1000
    while time.perf_counter() < _:
        pass


###########################
# MAIN CODE
###########################

if __name__ == "__main__":
    sch_app_main()
