#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
demo_app_msg

These classes will be used by other apps, when you change a field, make sure to change it elsewhere! (HK app and Comm app are likely users)

'''


###########################
#CLASSES
###########################
# stores out data for app to be provided to other apps
class demo_out_data(object):
    def __init__(self):
        self.very_valueable_output_dummy = 0

# stores data for outputting to hk. Usually diagnostic, can contain outdata
class demo_hk_data(object):
    def __init__(self):
        self.heartbeat = 0
        self.curent_error_code = 0


        # We can't really command much, so these are not necessary probably
        #self.command_received_count = 0
        #self.command_error_count = 0
        #self.error_count = 0
        #self.status = 0
        #self.dummy_valuable_item_to_track = None