#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
demo_app_header


'''

###########################
#IMPORT MODULES
###########################
import zmq
from pyfs.build.build_defs.build_config_file import *


###########################
#CLASSES
###########################
# used to store general stateful data about the class
class demo_workspace(object):
    def __init__(self):
        self.dummy = None

# used to store data related to communication with other apps
class demo_bus(object):
    def __init__(self):
        # start ZMQ client for SCH
        self.wakeup_context = zmq.Context()
        self.wakeup_socket = self.wakeup_context.socket(zmq.SUB)
        self.wakeup_socket.setsockopt(zmq.SUBSCRIBE, b"")
        self.wakeup_socket.connect("tcp://" + host_address + ":" + str(sch_app_port))

        # start ZMQ client for receiving commands
        self.command_context = zmq.Context()
        self.command_socket = self.command_context.socket(zmq.SUB)
        self.command_socket.setsockopt(zmq.SUBSCRIBE, b"")
        self.command_socket.bind("tcp://" + host_address + ":" + str(demo_app_cmd_in_port))

        # start ZMQ client for Publishing OutData
        self.out_data_context = zmq.Context()
        self.out_data_socket = self.out_data_context.socket(zmq.PUB)
        self.out_data_socket.bind("tcp://" + host_address + ":" + str(demo_app_outdata_port))

        # start ZMQ client for Publishing HK Tlm
        self.hk_data_context = zmq.Context()
        self.hk_data_socket = self.hk_data_context.socket(zmq.PUB)
        self.hk_data_socket.connect("tcp://" + host_address + ":" + str(hk_app_in_port))

# stores constant values for class
class demo_constants(object):
    def __init__(self):
        self.APP_NAME_STR = "DEMO_APP"




