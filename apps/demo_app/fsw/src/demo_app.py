#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
demo_app

 This is the highest level representation of your app
 Make other files and classes that are contained within or called from this one. Don't bloat this file!
 Philosophically, this file should mainly call other functions or do basic interfacing operations
 In CFS, this file would be responsible for initialization, processing data and commands, calling the main loop, and sending the outdata/housekeeping.
 If you are doing more than that here, consider moving it to another file and calling it from this one.
 In actual CFS, this file would probably not be a class, it would be a c file.
 Since Python doesn't have a way to scope variables to a file, we're using a class here to prevent having a ton of globals

'''

###########################
#IMPORT MODULES
###########################
import os
import sys
import json
import zmq

# import "header" file
top_dir = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))))))
sys.path.append(top_dir)
from pyfs.apps.demo_app.fsw.src.demo_app_header import *
from pyfs.apps.demo_app.fsw.mission_inc.demo_app_msg import *
from pyfs.apps.utils import sch_handling, base_paths


###########################
#CLASSES
###########################
class demo_app(object):
    def __init__(self):
        # common members, imported from utils
        self.paths = base_paths.base_paths(os.path.realpath(__file__))
        self.sch_handling = sch_handling.sch_handling()

        # app specific members, similar but different enough to warrant their own implementations
        # The workspace is used for information that will change overtime but you want it to persist between wakeups
        self.workspace = demo_workspace()
        self.bus = demo_bus()
        self.out_data = demo_out_data()
        self.hk_data = demo_hk_data()
        self.constants = demo_constants()

        #load input params
        iload_filename = os.path.join(self.paths.table_dir, 'iloads.json') #iload file name
        iload_file = open(iload_filename,)
        self.iloads = json.load(iload_file)

        # if you have a failure, set this to false
        self.init_success = True

    def main(self):
        #if init success, start main loop
        if self.init_success:
            self.receive_msg()
        else:
            print("DEMO_APP: Error initializing " + self.constants.APP_NAME_STR)

    def receive_msg(self):

        #this is the actual main loop of the program
        while True:
            # code will pause here until it receives a message from the socket
            message = self.bus.wakeup_socket.recv()
            # parses message when received, may consider using different wakeup sockets in the future to reduce these checks
            sch_msg_cmd, sch_cmd_timing = message.decode('utf-8').split('_FREQUENCY_')
        
            if sch_msg_cmd == 'SCH_GROUP_DEMO_WAKEUP':
                # Before executing wakeup calls, make sure we're not overrunning
                parsed_msg = self.sch_handling.sch_protect_wakeup_overrun(message, sch_msg_cmd, sch_cmd_timing)
                if(parsed_msg.sch_msg_cmd == 'SCH_GROUP_DEMO_WAKEUP'):
                    print("DEMO_APP: WAKEUP")
                    self.process_commands()
                    processed_data = self.process_data()
                    self.executive(processed_data)
                    # If you have a lot of postprocessing, make a "composer" function to prepare for out data and call it here
                    self.send_out_data()
            elif sch_msg_cmd == 'SCH_GROUP_DEMO_HK':
                # Before executing hk calls, make sure we're not overrunning
                parsed_msg = self.sch_handling.sch_protect_hk_overrun(message, sch_msg_cmd, sch_cmd_timing)
                if(parsed_msg.sch_msg_cmd == 'SCH_GROUP_DEMO_HK'):
                    self.report_housekeeping()

    def process_data(self):
        #print("DEMO_APP: process data")
        # process latest message output
        # processed_data = {}
       # try:
       #     while True:
            #     message = self.bus.control_in_socket.recv(zmq.DONTWAIT)
            #     message_json = message.decode('utf-8')
            #     processed_data = json.loads(message_json)
            #     print("DEMO_APP: received message dict: " + str(processed_data))
       # except zmq.error.Again:
       #     print("DEMO_APP: woke up with no new data")
       #     pass

        # process relevant demo data here, example in comments above
        processed_data = {"key": "val"}
        return processed_data



    def process_commands(self):
        #print("DEMO_APP: process commands")
        try:
            while True:
                message = self.bus.command_socket.recv(zmq.DONTWAIT)
                if message.decode('utf-8') == "DEMO_NOOP":
                    print("DEMO_APP: received No-Op command")
                #TODO: This is where you would process commands
        except zmq.error.Again:
            pass
        return 0

    def executive(self, processed_data_dict):
        #TODO: add stuff, probably have a different file where most of this is done to avoid bloat
        #print("DEMO_APP: process executive")

        #TODO: You would maybe store some essential elements of the processed data in your workspace if you need them on next call without new data

        #TODO: You would do your core functionality, using the processed data to effect some state

        #TODO: You would then store or return some of that modified state for reporting through some effector or the outdata and hk tlm

        #TODO: Probably store that data here and have a downstream function synthesize it into the right format
       
        return 0

    # Convert outdata class fields into a json string and send across the software bus
    # This is where you would do any final post processing as well.
    # Alternatively, if you have a lot of post processing, make a "composer" class/function
    def send_out_data(self):
        if self.out_data:
            json_str = json.dumps(self.out_data, default=vars)
            self.bus.out_data_socket.send(bytes(json_str, 'utf-8'))
            #print("DEMO_APP: send out data")

    # Convert hk class fields into a json string and send across the software bus
    def report_housekeeping(self):
        if self.hk_data:
            self.hk_data.heartbeat = self.hk_data.heartbeat + 1
            json_str = json.dumps(self.hk_data, default=vars)
            self.bus.hk_data_socket.send_multipart([bytes(self.constants.APP_NAME_STR, 'utf-8'), bytes(json_str, 'utf-8')])
            #print("DEMO_APP: send hk data")
            
            # Reset error code after reporting housekeeping. TODO If the HK packet drops, this info will be lost
            self.hk_data.curent_error_code = 0


###########################
# MAIN CODE
###########################
if __name__ == "__main__":
    demo = demo_app()
    demo.main()
