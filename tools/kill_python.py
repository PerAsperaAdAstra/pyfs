#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
kill_python

stops all Python processes

'''


###########################
#IMPORT MODULES
###########################
import sys
import os


###########################
# SUPPORT FUNCTIONS
###########################


###########################
# MAIN CODE
###########################
OS = sys.platform

if 'win' in OS:
   os.system('taskkill /IM python.exe /F')

if 'linux' in OS:
    os.system('pkill python3')